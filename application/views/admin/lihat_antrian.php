<?php $this->load->view('admin/common/_head_start') ?>
<link rel="stylesheet" href="<?= site_url('assets') ?>/admin/plugins/dt/datatables.min.css" />
<?php $this->load->view('admin/common/_head_end') ?>

<div class="wrapper">
    <?php $this->load->view('admin/common/navbar') ?>
    <?php $this->load->view('admin/common/sidebar') ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Lihat Antrian</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-12">
                        <div class="card">
                            <div class="card-body">
                            <table id="data_antrian" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Antrian pasien</th>
                                        <th>Nomor</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th>Antrian pasien</th>
                                        <th>Nomor</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </tfoot>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php $this->load->view('admin/common/footer') ?>
</div>

<?php $this->load->view('admin/common/_foot_start') ?>
<script src="<?= site_url('assets') ?>/admin/plugins/dt/datatables.min.js"></script>
<script>
    var dataTables = $('#data_antrian').DataTable({
        dom: 'Bfrtip',
        columns: [
            { data: "antrian_pasien" },
            { data: "nomor" },
            { data: "tanggal" }
        ]
    });

    loadData()

    function loadData(){
      dataTables.ajax.url( '<?= site_url() ?>ajax/antrian').load(function(data){
          console.log(data)
      })
    }
</script>
<?php $this->load->view('admin/common/_foot_end') ?>