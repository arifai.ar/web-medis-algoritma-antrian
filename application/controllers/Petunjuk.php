<?php defined('BASEPATH') or exit('no access');

class Petunjuk extends CI_Controller {
    function __construct(){
        parent::__construct();
    }

    function index(){
        //echo "menampilkan petunjuk penggunaan sistem untuk ditampilkan di webview android app";
        $this->load->view('general/petunjuk');
    }
}