<?php defined('BASEPATH') or exit('no access');

/**
 * created by Ahmad Rifa'i 
 * 25-01-2020
 * https://deerdeveloper.com
 */
class Dashboard extends CI_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->session->has_userdata('nama') || $this->session->userdata('nama') == NULL) redirect('login');
	}

	function index(){
		redirect('daftar-pasien');
	}

	function daftar_pasien(){
		$data = [
			"title" => "Daftar Pasien"
		];
		
		$this->load->view('admin/daftar_pasien', $data);
	}

	function jadwal_dokter(){
		$data = [
			"title" => "Jadwal Dokter"
		];
		
		$this->load->view('admin/jadwal_dokter', $data);
	}

	function riwayat_antrian(){
		$data = [
			"title" => "Riwayat Antrian"
		];
		
		$this->load->view('admin/riwayat_antrian', $data);
	}

	function lihat_antrian(){
		$data = [
			"title" => "Lihat Antrian"
		];
		
		$this->load->view('admin/lihat_antrian', $data);
	}

	function algoritma($debug=NULL){
		$this->load->model(['DataRiwayatAntrianModel','DataAntrianModel']);
		//var_dump($this->config->item('quantum'));
		$quantum = $this->config->item('quantum');
		//var_dump($this->DataRiwayatAntrianModel->get()->result_array());

		// $riwayatAntrianToday = $this->DataRiwayatAntrianModel->get()->result_array(); //untuk percobaan masih belum yang today
		## GET DATA - REAL TODAY
		$riwayatAntrianToday = $this->DataRiwayatAntrianModel->getToday()->result_array();

		/**
		 * Algoritma Perhitungan Mulai dari sini
		 */
		$this->load->library('AlgoritmaMFQ');
		if($debug != NULL) $this->algoritmamfq->setDebug($debug);

		##1 -- Menentukan 'Running Proses'
		$rp = $this->algoritmamfq->runningProses($riwayatAntrianToday, $quantum);

		##2 -- Menentukan 'Gantt Chart'
		$arr = $this->algoritmamfq->urutkanAsQuantum($rp, $quantum);
		$waktuM = $this->algoritmamfq->waktuMulai($arr);
		$waktuA = $this->algoritmamfq->waktuBerakhir($arr);

		##3 -- Menentukan 'Waiting Time'
		$wt = $this->algoritmamfq->waktuTunggu($riwayatAntrianToday, $quantum, $waktuM, $waktuA);

		##4 -- Menentukan 'Turn Around Time'
		$ta = $this->algoritmamfq->waktuKirakira($riwayatAntrianToday, $wt);

		##Langkah terakhir
		##Menggabungkan semua data dari proses sebelumnya, kemudian diurutkan (Ascending) berdasarkan TA
		$all = $this->algoritmamfq->gabunganSemuaProses($riwayatAntrianToday, $waktuM, $wt, $ta);

		$datas = [];
		$i = 0;
		foreach($all as $v){
			array_push($datas, [
				'antrian_pasien' => ++$i,
				'antrian_admin' => $this->session->id,
				'nomor' => $v['nomor'],
				'tanggal' => $v['tanggal']
			]);
		}

		//var_dump($datas);
		$this->DataAntrianModel->deleteAll();
		$this->DataAntrianModel->createBatch($datas);
	}
}