<?php defined('BASEPATH') or exit('no access');

class DataDokterModel extends CI_Model {

    private $table = 'jadwal_dokter';

    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function create($data){
        return $this->db->insert($this->table, $data);
    }

    function update($id, $data){
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    function get(){
        return $this->db->get($this->table);
    }

    function getByDay($day){
        $this->db->where('hari', $day);
        return $this->db->get($this->table);
    }

    function delete($id){
        return $this->db->delete($this->table, ["id" => $id]);
    }
}