<?php defined('BASEPATH') or exit('no access');

date_default_timezone_set('Asia/Jakarta');

class Api extends CI_Controller {
    function __construct(){
        parent::__construct();
    }

    function index(){
        $out = [
            "api" => "it's working",
            "version" => "1.0"
        ];

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($out));
    }

    function pasien(){
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == 'POST'){
            $out                        = [];
            $data                       = [];
            $data['nama_pasien']        = $this->input->post('nama');
            $data['umur']               = $this->input->post('umur');
            $data['jenis_kelamin']      = $this->input->post('jenis_kelamin');
            $data['no_hp']              = $this->input->post('no_hp');
            $data['alamat']             = $this->input->post('alamat');
            $data['agama']              = $this->input->post('agama');
            $data['penjamin']           = $this->input->post('penjamin');
            $data['tanggal_kunjungan']  = $this->input->post('tanggal_kunjungan');
            $data['status']             = $this->input->post('status');
            

            $this->load->model('DataPasienModel');
            
            if($this->DataPasienModel->create($data)){
                $out = [
                    "success" => TRUE,
                    "message" => "Sukses tambah pasien"
                ];
            } else {
                $out = [
                    "success" => FALSE,
                    "message" => "Gagal tambah pasien"
                ];
            }

            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($out));
        } else {
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(['error'=>'request is not found']));
        }
    }

    function dokter(){
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == 'GET'){
            $this->load->model('DataDokterModel');
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                [
                    "success" => TRUE,
                    "message" => "data dokter",
                    "data" => $this->DataDokterModel->get()->result_array()
                ]
            ));
        } else {
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(['error'=>'request is not found']));
        }
    }

    function test(){
        $days = ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'];
        $dw = date('w', strtotime(date('Y-m-d')));
        $this->load->model(['DataRiwayatAntrianModel', 'DataDokterModel']);
        $dokter = $this->DataDokterModel->getByDay($days[$dw-1]);
        $this->output    
            ->set_content_type('application/json')
            ->set_output(json_encode([
                "success" => TRUE,
                "message" => "Data riwayat antrian hari ini",
                "data" => $dokter
            ]));   
    }

    function riwayat_antrian(){
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == 'GET'){
            $days = ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'];
            $this->load->model(['DataRiwayatAntrianModel', 'DataDokterModel']);
            $antrianHariIni = $this->DataRiwayatAntrianModel->getToday()->result_array();
            $quantum = $this->config->item('quantum');
            $this->load->library('AlgoritmaMFQ');
            $this->algoritmamfq->setDebug(FALSE);

            ##1 -- Menentukan 'Running Proses'
            $rp = $this->algoritmamfq->runningProses($antrianHariIni, $quantum);

            ##2 -- Menentukan 'Gantt Chart'
            $arr = $this->algoritmamfq->urutkanAsQuantum($rp, $quantum);
            $waktuM = $this->algoritmamfq->waktuMulai($arr);
            $waktuA = $this->algoritmamfq->waktuBerakhir($arr);

            ##3 -- Menentukan 'Waiting Time'
            $wt = $this->algoritmamfq->waktuTunggu($antrianHariIni, $quantum, $waktuM, $waktuA);

            ##4 -- Menentukan 'Turn Around Time'
            $ta = $this->algoritmamfq->waktuKirakira($antrianHariIni, $wt);

            ##Langkah terakhir
            ##Menggabungkan semua data dari proses sebelumnya, kemudian diurutkan (Ascending) berdasarkan TA
            $all = $this->algoritmamfq->gabunganSemuaProses($antrianHariIni, $waktuM, $wt, $ta);
            
            $dw = date('w', strtotime(date('Y-m-d')));
            
            $dokter = $this->DataDokterModel->getByDay($days[$dw]);
    
            $jam = ($dokter->num_rows() > 0) ? $dokter->row_array()['jam_buka'] : '08:00:00';
            $datas = [];
            $i = 0;
            foreach($all as $v){
                $v['jam_buka'] = $jam;
                array_push($datas, $v);
            }
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode([
                "success" => TRUE,
                "message" => "Data riwayat antrian hari ini",
                "data" => $datas
            ]));
        } else {
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(['error'=>'request is not found']));
        }
    }
}