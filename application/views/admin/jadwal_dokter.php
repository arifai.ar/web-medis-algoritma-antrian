<?php $this->load->view('admin/common/_head_start') ?>
<link rel="stylesheet" href="<?= site_url('assets') ?>/admin/plugins/dt/datatables.min.css" />
<?php $this->load->view('admin/common/_head_end') ?>

<div class="wrapper">
    <?php $this->load->view('admin/common/navbar') ?>
    <?php $this->load->view('admin/common/sidebar') ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Jadwal Dokter</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6 text-right">
                        <button class="btn btn-primary" onclick="tambahData()"><i class="fas fa-plus"></i> Tambah Dokter</button>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-12">
                        <div class="card">
                            <div class="card-body">
                            <table id="data_dokter" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Hari</th>
                                        <th>Jam Buka</th>
                                        <th>Jam Tutup</th>
                                        <th>Dokter</th>
                                        <th>Kuota</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Hari</th>
                                        <th>Jam Buka</th>
                                        <th>Jam Tutup</th>
                                        <th>Dokter</th>
                                        <th>Kuota</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php $this->load->view('admin/common/footer') ?>
</div>

<div class="modal fade" id="modalTambahData">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <form role="form" id="form_tambah_data">
      <div class="modal-header">
          <h4 class="modal-title">Tambah Data Dokter</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Hari</label>
          <!-- <input type="text" class="form-control" placeholder="Hari" name="hari"> -->
          <select class="form-control" name="hari">
            <option value="Senin">Senin</option>
            <option value="Selasa">Selasa</option>
            <option value="Rabu">Rabu</option>
            <option value="Kamis">Kamis</option>
            <option value="Jumat">Jumat</option>
            <option value="Sabtu">Sabtu</option>
            <option value="Minggu">Minggu</option>
          </select>
        </div>
        <div class="form-group">
          <label>Jam Buka-Tutup</label>
          <div class="row">
            <div class="col-6">
              <div class="input-group date" id="timepicker1" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#timepicker1" name="jam_buka" />
                <div class="input-group-append" data-target="#timepicker1" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="far fa-clock"></i></div>
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="input-group date" id="timepicker2" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#timepicker2" name="jam_tutup" />
                <div class="input-group-append" data-target="#timepicker2" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="far fa-clock"></i></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Nama Dokter</label>
          <input type="text" class="form-control" placeholder="Nama dokter" name="dokter">
        </div>
        <div class="form-group">
          <label>Kuota</label>
          <input type="number" class="form-control col-6" placeholder="Kuota" name="kuota">
        </div>
      </div>
      <div class="modal-footer">
        <button onclick="tambahDataAction(event)" class="btn btn-success">Simpan</button>
      </div>
    </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modalEdit">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <form role="form" id="formEdit">
      <div class="modal-header">
          <h4 class="modal-title">Edit Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">
        <div class="form-group">
          <label>Hari</label>
          <!-- <input type="text" class="form-control" placeholder="Hari" name="hari"> -->
          <select class="form-control" name="hari">
            <option value="Senin">Senin</option>
            <option value="Selasa">Selasa</option>
            <option value="Rabu">Rabu</option>
            <option value="Kamis">Kamis</option>
            <option value="Jumat">Jumat</option>
            <option value="Sabtu">Sabtu</option>
            <option value="Minggu">Minggu</option>
          </select>
        </div>
        <div class="form-group">
          <label>Jam Buka-Tutup</label>
          <div class="row">
            <div class="col-6">
              <div class="input-group date" id="timepicker3" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#timepicker3" name="jam_buka" />
                <div class="input-group-append" data-target="#timepicker3" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="far fa-clock"></i></div>
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="input-group date" id="timepicker4" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#timepicker4" name="jam_tutup" />
                <div class="input-group-append" data-target="#timepicker4" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="far fa-clock"></i></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Nama Dokter</label>
          <input type="text" class="form-control" placeholder="Nama dokter" name="dokter">
        </div>
        <div class="form-group">
          <label>Kuota</label>
          <input type="number" class="form-control col-6" placeholder="Kuota" name="kuota">
        </div>
        
      </div>
      <div class="modal-footer">
        <button onclick="editAction(event)" class="btn btn-success">Ubah</button>
      </div>
    </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php $this->load->view('admin/common/_foot_start') ?>
<script src="<?= site_url('assets') ?>/admin/plugins/dt/datatables.min.js"></script>
<script src="<?= site_url('assets') ?>/admin/plugins/daterangepicker/daterangepicker.js"></script>
<script>
    var dataTables = $('#data_dokter').DataTable({
        dom: 'Bfrtip',
        columns: [
            { data: "id" },
            { data: "hari" },
            { data: "jam_buka" },
            { data: "jam_tutup" },
            { data: "dokter" },
            { data: "kuota" },
            {
                data: null,
                defaultContent: '<center><div class="btn-group"> <a href="#" onclick="edit(this)" class="btn btn-warning btn-sm">Edit</a>  <a href="#" onclick="hapus(this)" class="btn btn-danger btn-sm">Delete</a></div></center>'
            }
        ]
    });

    loadData()

    function loadData(){
      dataTables.ajax.url( '<?= site_url() ?>ajax/dokter').load(function(data){
          console.log(data)
      })
    }

    function tambahData(){
        $('#modalTambahData').modal('show')
    }

    function tambahDataAction(e){
      e.preventDefault()
      console.log($('#form_tambah_data').serialize())
      $.ajax({
        type    : 'POST',
        data    : $('#form_tambah_data').serialize(),
        url     : '<?= site_url('ajax/dokter') ?>',
        success : function(data){
          if(data.success){
            loadData()
            $('#form_tambah_data')[0].reset()
            $('#modalTambahData').modal('hide')
          } else {
            alert('ada kesalahan '+data.message)
          }
        }
      })
    }

    function hapus(t){
      var id = $($(t).parent().parent().parent().parent().find('td')[0]).text()
        if(confirm('Apakah anda yakin ?')){
            $.ajax({
                type : 'DELETE',
                url : '<?= site_url('ajax/dokter/') ?>'+id,
                success : function(data){
                    if(data.success){
                        loadData()
                    }
                }
            })
        }
    }

    function edit(t){
      var id = $($(t).parent().parent().parent().parent().find('td')[0]).text()
      var hari = $($(t).parent().parent().parent().parent().find('td')[1]).text()
      var jam_buka = $($(t).parent().parent().parent().parent().find('td')[2]).text()
      var jam_tutup = $($(t).parent().parent().parent().parent().find('td')[3]).text()
      var dokter = $($(t).parent().parent().parent().parent().find('td')[4]).text()
      var kuota = $($(t).parent().parent().parent().parent().find('td')[5]).text()

      var modal = $('#modalEdit');

      modal.find('input[name=id]').val(id)
      modal.find('select[name=hari]').val(hari)
      modal.find('input[name=jam_buka]').val(jam_buka)
      modal.find('input[name=jam_tutup]').val(jam_tutup)
      modal.find('input[name=dokter]').val(dokter)
      modal.find('input[name=kuota]').val(kuota)

      modal.modal('show')
    }

    function editAction(e){
        e.preventDefault()
        var data = $('#formEdit').serialize()
        $.ajax({
            type    : 'PUT',
            url     : '<?= site_url('ajax/dokter') ?>',
            data    : data,
            success : function(data){
                if(data.success){
                    $('#modalEdit').modal('hide')
                    loadData()
                    alert('Sukses update data')
                } else {
                    alert('Gagal update data')
                }
            },
            error : function(err){
                console.log(err)
            }
        })
    }

    $('#timepicker1, #timepicker2, #timepicker3, #timepicker4').datetimepicker({
      format: 'HH:mm'
    })
</script>
<?php $this->load->view('admin/common/_foot_end') ?>