<?php defined('BASEPATH') or exit('no access');

class LoginModel extends CI_Model {

    private $table = 'login';

    private $fields = ['Nama', 'Password'];

    function __construct(){
        parent::__construct();
    }

    function auth($username, $password){
        $this->load->database();
        $this->db->select('*');
        $this->db->where($this->fields[0], $username);
        $this->db->where($this->fields[1], $password);
        return $this->db->get($this->table);
    }

    function setLoginSession($login){
        $this->session->set_userdata('nama', $login->row()->nama);
        $this->session->set_userdata('id', $login->row()->id);
    }

    function removeLoginSession(){
        $this->session->unset_userdata('nama');
        $this->session->sess_destroy();
    }
}
