  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?= site_url('assets') ?>/general/images/logov1.png" alt="Medis Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">RSIA Tinatapura</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="<?= site_url('daftar-pasien') ?>" class="nav-link <?= ($this->uri->segment(1) == 'daftar-pasien') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-child"></i>
              <p>
                Daftar Pasien
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url('jadwal-dokter') ?>" class="nav-link <?= ($this->uri->segment(1) == 'jadwal-dokter') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-user-md"></i>
              <p>
                Jadwal Dokter
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url('riwayat-antrian') ?>" class="nav-link <?= ($this->uri->segment(1) == 'riwayat-antrian') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-tasks"></i>
              <p>
                Riwayat Antrian
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url('lihat-antrian') ?>" class="nav-link <?= ($this->uri->segment(1) == 'lihat-antrian') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Lihat Antrian
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url('algoritma') ?>" class="nav-link <?= ($this->uri->segment(1) == 'algoritma') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Algoritma
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>