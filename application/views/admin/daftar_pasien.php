<?php $this->load->view('admin/common/_head_start') ?>
<link rel="stylesheet" href="<?= site_url('assets') ?>/admin/plugins/dt/datatables.min.css" />
<?php $this->load->view('admin/common/_head_end') ?>

<div class="wrapper">
    <?php $this->load->view('admin/common/navbar') ?>
    <?php $this->load->view('admin/common/sidebar') ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Daftar Pasien</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-12">
                        <div class="card">
                            <div class="card-body">
                            <table id="data_pasien" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama Pasien</th>
                                        <th>Alamat</th>
                                        <th>Umur</th>
                                        <th>Jenis Kelamin</th>
                                        <th>No. HP</th>
                                        <th>Agama</th>
                                        <th>Penjamin</th>
                                        <th>Tgl Kunjungan</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama Pasien</th>
                                        <th>Alamat</th>
                                        <th>Umur</th>
                                        <th>Jenis Kelamin</th>
                                        <th>No. HP</th>
                                        <th>Agama</th>
                                        <th>Penjamin</th>
                                        <th>Tgl Kunjungan</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php $this->load->view('admin/common/footer') ?>
</div>

<div class="modal fade" id="modalEdit">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <form role="form" id="formEdit">
      <div class="modal-header">
          <h4 class="modal-title">Edit Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">
        <div class="form-group">
          <label>Nama</label>
          <input type="text" class="form-control" placeholder="Nama" name="nama">
        </div>

        <div class="form-group">
          <label>Alamat</label>
          <input type="text" class="form-control" placeholder="Alamat" name="alamat">
        </div>

        <div class="form-group">
          <label>Umur</label>
          <input type="text" class="form-control" placeholder="Umur" name="umur">
        </div>

        <div class="form-group">
          <label>No. HP</label>
          <input type="text" class="form-control" placeholder="No. HP" name="no_hp">
        </div>

        <div class="form-group">
          <label>Penjamin</label>
          <input type="text" class="form-control" placeholder="Penjamin" name="penjamin">
        </div>

        <div class="form-group">
          <label>Tanggal Kunjungan</label>
          <input type="date" class="form-control" placeholder="Tanggal kunjungan" name="tanggal_kunjungan">
        </div>

        <div class="form-group">
          <label>Status</label>
          <!-- <input type="text" class="form-control" placeholder="Status" name="status"> -->
          <select class="form-control" name="status">
            <option value="Kronis">Kronis</option>
            <option value="Sedang">Sedang</option>
            <option value="Tidak kronis">Tidak kronis</option>
          </select>
        </div>
        
      </div>
      <div class="modal-footer">
        <button onclick="editAction(event)" class="btn btn-success">Ubah</button>
      </div>
    </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php $this->load->view('admin/common/_foot_start') ?>
<script src="<?= site_url('assets') ?>/admin/plugins/dt/datatables.min.js"></script>
<script>
    var dataTables = $('#data_pasien').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        columns: [
            { data: "id" },
            { data: "nama_pasien" },
            { data: "alamat" },
            { data: "umur" },
            { data: "jenis_kelamin" },
            { data: "no_hp" },
            { data: "agama" },
            { data: "penjamin" },
            { data: "tanggal_kunjungan" },
            { data: "status" },
            {
                data: null,
                defaultContent: '<center><div class="btn-group"> <a href="#" onclick="edit(this)" class="btn btn-warning btn-sm">Edit</a>  <a href="#" onclick="hapus(this)" class="btn btn-danger btn-sm">Delete</a></div></center>'
            }
        ]
    });

    loadData()

    function loadData(){
        dataTables.ajax.url( '<?= site_url() ?>ajax/pasien').load(function(data){
            console.log(data)
        })
    }

    function hapus(t){
        var id = $($(t).parent().parent().parent().parent().find('td')[0]).text()
        if(confirm('Apakah anda yakin ?')){
            $.ajax({
                type : 'DELETE',
                url : '<?= site_url('ajax/pasien/') ?>'+id,
                success : function(data){
                    if(data.success){
                        loadData()
                    }
                }
            })
        }
    }

    function edit(t){
        var id = $($(t).parent().parent().parent().parent().find('td')[0]).text()
        var nama = $($(t).parent().parent().parent().parent().find('td')[1]).text()
        var alamat = $($(t).parent().parent().parent().parent().find('td')[2]).text()
        var umur = $($(t).parent().parent().parent().parent().find('td')[3]).text()
        var noHp = $($(t).parent().parent().parent().parent().find('td')[5]).text()
        var penjamin = $($(t).parent().parent().parent().parent().find('td')[7]).text()
        var tglKunjungan = $($(t).parent().parent().parent().parent().find('td')[8]).text()
        var status = $($(t).parent().parent().parent().parent().find('td')[9]).text()
        //alert('Edit feature not yet')
        var modal = $('#modalEdit')
        modal.modal('show')

        modal.find('input[name=id]').val(id)
        modal.find('input[name=nama]').val(nama)
        modal.find('input[name=alamat]').val(alamat)
        modal.find('input[name=umur]').val(umur)
        modal.find('input[name=no_hp]').val(noHp)
        modal.find('input[name=penjamin]').val(penjamin)
        modal.find('input[name=tanggal_kunjungan]').val(tglKunjungan)
        modal.find('select[name=status]').val(status)
    }

    function editAction(e){
        e.preventDefault()
        var data = $('#formEdit').serialize()
        $.ajax({
            type    : 'PUT',
            url     : '<?= site_url('ajax/pasien') ?>',
            data    : data,
            success : function(data){
                if(data.success){
                    $('#modalEdit').modal('hide')
                    loadData()
                    alert('Sukses update data')
                } else {
                    alert('Gagal update data')
                }
            },
            error : function(err){
                console.log(err)
            }
        })
    }
</script>
<?php $this->load->view('admin/common/_foot_end') ?>