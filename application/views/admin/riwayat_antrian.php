<?php $this->load->view('admin/common/_head_start') ?>
<link rel="stylesheet" href="<?= site_url('assets') ?>/admin/plugins/dt/datatables.min.css" />
<?php $this->load->view('admin/common/_head_end') ?>

<div class="wrapper">
    <?php $this->load->view('admin/common/navbar') ?>
    <?php $this->load->view('admin/common/sidebar') ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Riwayat Antrian</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6 text-right">
                        <button class="btn btn-primary" onclick="tambahData()"><i class="fas fa-plus"></i> Tambah Data</button>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-12">
                        <div class="card">
                            <div class="card-body">
                            <table id="data_riwayat" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama</th>
                                        <th>Nomor</th>
                                        <th>BT</th>
                                        <th>AT</th>
                                        <th>Tanggal</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama</th>
                                        <th>Nomor</th>
                                        <th>BT</th>
                                        <th>AT</th>
                                        <th>Tanggal</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php $this->load->view('admin/common/footer') ?>
</div>

<div class="modal fade" id="modalTambahData">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <form role="form" id="form_tambah_data">
      <div class="modal-header">
          <h4 class="modal-title">Tambah Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Nama</label>
          <!-- <input type="text" class="form-control" placeholder="Nama" name="nama"> -->
          <select class="form-control" name="nama"></select>
        </div>
        <div class="form-group">
          <label>Nomor</label>
          <input type="number" class="form-control" placeholder="Nomor" name="nomor">
        </div>
        <div class="form-group">
          <label>BT</label>
          <input type="number" class="form-control col-6" placeholder="BT" name="bt">
        </div>
        <div class="form-group">
          <label>AT</label>
          <input type="number" class="form-control col-6" placeholder="AT" name="at">
        </div>
        <div class="form-group">
          <label>Tanggal</label>
          <input type="date" class="form-control" placeholder="Tanggal" name="tanggal">
        </div>
      </div>
      <div class="modal-footer">
        <button onclick="tambahDataAction(event)" class="btn btn-success">Simpan</button>
      </div>
    </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modalEdit">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <form role="form" id="formEdit">
      <div class="modal-header">
          <h4 class="modal-title">Edit Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">
        <div class="form-group">
          <label>Nama</label>
          <!-- <input type="text" class="form-control" placeholder="Nama" name="nama"> -->
          <select class="form-control" name="nama"></select>
        </div>
        <div class="form-group">
          <label>Nomor</label>
          <input type="number" class="form-control" placeholder="Nomor" name="nomor">
        </div>
        <div class="form-group">
          <label>BT</label>
          <input type="number" class="form-control col-6" placeholder="BT" name="bt">
        </div>
        <div class="form-group">
          <label>AT</label>
          <input type="number" class="form-control col-6" placeholder="AT" name="at">
        </div>
        <div class="form-group">
          <label>Tanggal</label>
          <input type="date" class="form-control" placeholder="Tanggal" name="tanggal">
        </div>
        
      </div>
      <div class="modal-footer">
        <button onclick="editAction(event)" class="btn btn-success">Ubah</button>
      </div>
    </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php $this->load->view('admin/common/_foot_start') ?>
<script src="<?= site_url('assets') ?>/admin/plugins/dt/datatables.min.js"></script>
<script src="<?= site_url('assets') ?>/admin/plugins/daterangepicker/daterangepicker.js"></script>
<script>
    var dataTables = $('#data_riwayat').DataTable({
        dom: 'Bfrtip',
        columns: [
            { data: "id" },
            { data: "nama" },
            { data: "nomor" },
            { data: "bt" },
            { data: "at" },
            { data: "tanggal" },
            {
                data: null,
                defaultContent: '<center><div class="btn-group"> <a href="#" onclick="edit(this)" class="btn btn-warning btn-sm">Edit</a>  <a href="#" onclick="hapus(this)" class="btn btn-danger btn-sm">Delete</a></div></center>'
            }
        ]
    });

    loadData()
    getDataPasien()

    function loadData(){
        dataTables.ajax.url( '<?= site_url() ?>ajax/riwayat-antrian').load(function(data){
            console.log(data)
        })
    }

    function tambahData(){
        $('#modalTambahData').modal('show')
    }

    function tambahDataAction(e){
        e.preventDefault()
        $.ajax({
            type    : 'POST',
            url     : '<?= site_url('ajax/riwayat-antrian') ?>',
            data    : $('#form_tambah_data').serialize(),
            success : function(data){
                if(data.success){
                    $('#form_tambah_data')[0].reset()
                    $('#modalTambahData').modal('hide')
                    loadData()
                    updateAntrianDenganAlgoritma()
                } else {
                    alert('Ada kesalahan : '+data.message)
                }
            }
        })
    }

    function updateAntrianDenganAlgoritma(){
        $.ajax({
            type    : 'PUT',
            url     : '<?= site_url('ajax/antrian') ?>',
            success : function(data){
                if(data.success){
                    alert('sukses update antrian')
                } else {
                    console.log('Ada masalah update antrian')
                }
            }
        })
    }

    function hapus(t){
      var id = $($(t).parent().parent().parent().parent().find('td')[0]).text()
        if(confirm('Apakah anda yakin ?')){
            $.ajax({
                type : 'DELETE',
                url : '<?= site_url('ajax/riwayat-antrian/') ?>'+id,
                success : function(data){
                    if(data.success){
                        loadData()
                    }
                }
            })
        }
    }

    function edit(t){
        var id = $($(t).parent().parent().parent().parent().find('td')[0]).text()
        var nama = $($(t).parent().parent().parent().parent().find('td')[1]).text()
        var nomor = $($(t).parent().parent().parent().parent().find('td')[2]).text()
        var bt = $($(t).parent().parent().parent().parent().find('td')[3]).text()
        var at = $($(t).parent().parent().parent().parent().find('td')[4]).text()
        var tanggal = $($(t).parent().parent().parent().parent().find('td')[5]).text()
        
        var modal = $('#modalEdit')
        modal.modal('show')

        modal.find('input[name=id]').val(id)
        modal.find('select[name=nama]').val(nama)
        modal.find('input[name=nomor]').val(nomor)
        modal.find('input[name=bt]').val(bt)
        modal.find('input[name=at]').val(at)
        modal.find('input[name=tanggal]').val(tanggal)
    }

    function editAction(e){
        e.preventDefault()
        var data = $('#formEdit').serialize()
        $.ajax({
            type    : 'PUT',
            url     : '<?= site_url('ajax/riwayat-antrian') ?>',
            data    : data,
            success : function(data){
                if(data.success){
                    $('#modalEdit').modal('hide')
                    loadData()
                    updateAntrianDenganAlgoritma()
                    alert('Sukses update data')
                } else {
                    alert('Gagal update data')
                }
            },
            error : function(err){
                console.log(err)
            }
        })
    }

    function getDataPasien(){
        var selopt = $('#modalTambahData').find('select[name=nama]')
        var selopt1 = $('#modalEdit').find('select[name=nama]')
    
        selopt.html('')
        selopt1.html('')
        selopt.append('<option>Pilih pasien</option>')
        selopt1.append('<option>Pilih pasien</option>')
        $.get('<?= site_url('ajax/pasien') ?>', function(d){
            console.log(d)
            d.data.forEach(function(v){
                console.log(v)
                selopt.append(`<option value='${v.nama_pasien}'>${v.nama_pasien}</option>`)
                selopt1.append(`<option value='${v.nama_pasien}'>${v.nama_pasien}</option>`)
            })
        })
    }
</script>
<?php $this->load->view('admin/common/_foot_end') ?>