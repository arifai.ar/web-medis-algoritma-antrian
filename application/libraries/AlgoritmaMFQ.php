<?php defined('BASEPATH') or exit('no access');

class AlgoritmaMFQ {
    // untuk menampilkan proses algoritma set TRUE
    private $DEBUG = TRUE;
    private $ci; 
    
    function __construct(){
        $this->ci = & get_instance(); 
    }

    public function setDebug($bol){
        $this->DEBUG = $bol;
    }
    
    public function runningProses($arr, $quantum){
		$out = [];
		for($i = 0; $i < count($arr); $i++){
			$temp = [];
			$sisaOld = intval($arr[$i]['bt']);
			for($j = 0; $j < count($quantum); $j++){
				$sisaNew = $sisaOld - intval($quantum[$j]);
				if($j != count($quantum)-1){
					if($sisaNew >= 0){
						$sisaOld = $sisaNew;
						array_push($temp, intval($quantum[$j]));		
					}
				} else {
					if($sisaOld > 0) array_push($temp, $sisaOld);
				}
			}
			array_push($out, $temp);
		}
		if($this->DEBUG) {
			echo '----- #1 RUNNING PROSES ----------------------------- <br>';
			echo '<pre>' . var_export($out, true) . '</pre>';
		}
		return $out;
	}

	public function urutkanAsQuantum($run, $quantum){
		$out = [];
		for($q = 0; $q < count($quantum); $q++){
			$temp = [];
			for($i = 0; $i < count($run); $i++){
				if($q < count($run[$i])){
					// echo "COUNT  =  ".$i.' => '.count($run[$i]).'<br>';
					$key = 'P'.strval($i+1);
					$temp[$key] = $run[$i][$q];
				}
			}
			//echo '-------------------------<br>';
			$str = 'Q'.strval($q+1);
			$out[$str] = $temp;
		}
		if($this->DEBUG){ 
			echo '----- #2 GANTT CHART ----------------------------- <br>';
			echo '<pre>' . var_export($out, true) . '</pre>';
		}
		return $out;
	}

	public function waktuMulai($arr){
		$out = [];
		$jumlah = 0;
		$i = 0;
		foreach($arr as $k => $v){
			$temp = [];
			$j = 0;
			$keys = array_keys($v);
			// echo '<pre>'.var_export($keys, true).'</pre>';
			foreach($v as $k1 => $v1){
				if($i == 0 && $j == 0){
					$temp[$k1] = 0;
					$jumlah += 0;
				} 
				else if($j == 0){
					$jumlah = $this->totalWaktuTerakhirDariQ($arr, $i-1);
					$temp[$k1] = $jumlah;
					// echo  "Q{".strval($i+1)."} EKSEKUSI-{".strval($j+1)."} = {$jumlah} <br>";
				} 
				else {
					// echo "V = {$v[$keys[$j-1]]} <br>";
					$jumlah += $v[$keys[$j-1]];
					$temp[$k1] = $jumlah;
				}
				$j++;
			}
			$out[$k] = $temp;
			$i++;
		}
		if($this->DEBUG) {
			echo '----- #2.1 GANTT CHART ~ Start Time ----------------------------- <br>';
			echo '<pre>' . var_export($out, true) . '</pre>';
		}
		return $out;
	}

	public function waktuBerakhir($arr){
		$out = [];
		$jumlah = 0;
		$i = 0;
		foreach($arr as $k => $v){
			$temp = [];
			$j = 0;
			$keys = array_keys($v);
			foreach($v as $k1 => $v1){
				if($i == 0 && $j == 0){
					$temp[$k1] = $v1;
					$jumlah += $v1;
				}
				else if($j == 0){
					$jumlah = $this->totalWaktuTerakhirDariQ($arr, $i-1);
					$jumlah += $v1;
					$temp[$k1] = $jumlah;
				} else {
					$jumlah += $v1;
					$temp[$k1] = $jumlah;
				}
				$j++;
			}
			$out[$k] = $temp;
			$i++;
		}
		if($this->DEBUG) {
			echo '----- #2.2 GANTT CHART ~ End Time ----------------------------- <br>';
			echo '<pre>' . var_export($out, true) . '</pre>';
		}
		return $out;
	}

	public function totalWaktuTerakhirDariQ($arr, $index){
		static $total = 0;
		if($index >= 0){
			$idx = $index;
			$keys = 'Q'.strval($idx+1);
			foreach($arr[$keys] as $v){
				$total += $v;
			}
			return $this->totalWaktuTerakhirDariQ($arr, $index-1);
		} else {
			$newTotal = $total;
			$total = 0;
			return $newTotal;
		}
	}

	public function waktuTunggu($allproses, $quantum, $mulai, $akhir){
		$out = [];
		for($i = 0; $i < count($allproses); $i++){
			$key = 'WTP'.strval($i+1);
			$jumlah = 0;
			for($j = 0; $j < count($quantum); $j++){
				if($j == 0){
					// jika quantum == Q1 maka waktu waktu awal - waktu kedatangan
					$qk = 'Q'.strval($j+1);
					$pk = 'P'.strval($i+1);
					if(array_key_exists($pk, $mulai[$qk])){
						$jumlah += $mulai[$qk][$pk] - intval($allproses[$i]['at']);
					}
				} else {
					// jika quantum > Q1 maka waktu awal diquantum tersebut - waktu akhir di quantum sebelumnya
					$qk = 'Q'.strval($j+1);
					$qkPrev = 'Q'.strval($j);
					$pk = 'P'.strval($i+1);
					if(array_key_exists($pk, $mulai[$qk]) && array_key_exists($pk, $mulai[$qkPrev])){
						$jumlah += $mulai[$qk][$pk] - $akhir[$qkPrev][$pk];
					}
				}
			}
			$out[$key] = $jumlah;
		}
		if($this->DEBUG) {
			echo '----- #3 WAITING TIME ----------------------------- <br>';
			echo '<pre>' . var_export($out, true) . '</pre>';
		}
		return $out;
	}

	public function waktuKirakira($allproses, $wt){
		$out = [];
		for($i = 0; $i < count($allproses); $i++){
			$out['TAP'.strval($i+1)] = intval($allproses[$i]['bt'])+$wt['WTP'.strval($i+1)];
		}
		if($this->DEBUG) {
			echo '----- #4 TURN ARIVALD TIME ----------------------------- <br>';
			echo '<pre>' . var_export($out, true) . '</pre>';
		}
		return $out;
	}

	public function gabunganSemuaProses($allproses, $waktuA, $wt, $ta){
		$out = [];
		for($i = 0; $i < count($allproses); $i++){
			array_push($out, [
				'nama' => $allproses[$i]['nama'],
                'tanggal' => $allproses[$i]['tanggal'],
                'nomor' => $allproses[$i]['nomor'],
				'bt' => $allproses[$i]['bt'],
				'at' => intval($allproses[$i]['at']),
				'rt' => $waktuA['Q1']['P'.strval($i+1)]-intval($allproses[$i]['at']),
				'wt' => $wt['WTP'.strval($i+1)],
				'ta' =>  $ta['TAP'.strval($i+1)]
			]);
		}
		// urutkan berdasarkan TA = Turn Around Time
		$o = $this->sort_by($out, 'ta', 'asc');
		if($this->DEBUG) {
			echo '----- #FINAL PROCESS ----------------------------- <br>';
			echo '<pre>' . var_export($o, true) . '</pre>';
		}
		return $o;
	}

	public function sort_by($array,  $keyname = null, $sortby) {
		$myarray = $inarray = array();   
		foreach ($array as $i => $befree) {
			$myarray[$i] = $array[$i][$keyname];
		}
		switch ($sortby) {
		case 'asc':
			asort($myarray);
			break;
		case 'arsort':
			arsort($myarray);
			break;
		case 'natcasesor':
			natcasesort($myarray);
			break;
		}
		foreach ( $myarray as $key=> $befree) {
			$inarray[$key] = $array[$key];
		}
		return $inarray;
	}
}