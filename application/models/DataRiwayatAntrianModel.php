<?php defined('BASEPATH') or exit('no access');

class DataRiwayatAntrianModel extends CI_Model {
    private $table = 'riwayat_antrian';

    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function create($data){
        return $this->db->insert($this->table, $data);
    }

    function update($id, $data){
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    function get(){
        return $this->db->get($this->table);
    }

    function getToday(){
        $this->db->where('tanggal', date('Y-m-d'));
        return $this->db->get($this->table);
    }

    function getTodayWhere($nama, $tanggal){
        $this->db->where('tanggal', date('Y-m-d', strtotime($tanggal)));
        $this->db->where('nama', $nama);
        return $this->db->get($this->table);
    }

    function delete($id){
        return $this->db->delete($this->table, ["id" => $id]);
    }
}