<?php defined('BASEPATH') or exit('no access');

class DataAntrianModel extends CI_Model {
    private $table = 'antrian';

    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function create($data){
        return $this->db->insert($this->table, $data);
    }

    function createBatch($datas){
        if(count($datas) != 0) return $this->db->insert_batch($this->table, $datas);
    }

    function update($id, $data){
        
    }

    function get(){
        return $this->db->get($this->table);
    }

    function delete($id){
        return $this->db->delete($this->table, ["id" => $id]);
    }

    function deleteAll(){
        return $this->db->truncate($this->table);
    }
}