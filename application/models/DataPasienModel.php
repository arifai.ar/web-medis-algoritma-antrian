<?php defined('BASEPATH') or exit('no access');

class DataPasienModel extends CI_Model {

    private $table = "data_pasien";

    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function create($data){
        return $this->db->insert($this->table, $data);
    }

    function update($id, $data){
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    function get(){
        return $this->db->get($this->table);
    }

    function delete($id){
        return $this->db->delete($this->table, ["id" => $id]);
    }
}