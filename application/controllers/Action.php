<?php defined('BASEPATH') or exit('no access');

class Action extends CI_Controller {
    function __construct(){
        parent::__construct();
    }

    function auth(){
        $this->load->model('LoginModel');

        $u = $this->input->post('username');
        $p = $this->input->post('password');

        $login = $this->LoginModel->auth($u, md5($p));
        if($login->num_rows() == 1){
            //echo "sukses";
            $this->LoginModel->setLoginSession($login);
            redirect('dashboard');
        } else {
            //echo "gagal";
            redirect('login?error');
        }
    }
}