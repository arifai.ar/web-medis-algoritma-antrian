<?php defined('BASEPATH') or exit('no access');

/**
 * 
 */
class Auth extends CI_Controller {
	function __construct(){
		parent::__construct();
	}

	function index(){
		redirect('login');
	}

	function login(){
		if($this->session->has_userdata('nama') && $this->session->userdata('nama') != NULL) redirect('dashboard');
		$this->load->view('general/login');	
	}

	function logout(){
		session_destroy();
		var_dump($_SESSION);
		redirect('login');
	}
}