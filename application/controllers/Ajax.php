<?php defined('BASEPATH') or exit('no access');

class Ajax extends CI_Controller {
    function __construct(){
        parent::__construct();
    }

    function index(){
        $out = [
            "ajax" => "it's working",
            "version" => "1.0"
        ];

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($out));
    }

    function pasien($id=NULL){
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == 'PUT'){
            parse_str(file_get_contents('php://input'), $_PUT);
            //var_dump($_PUT); //$_PUT contains put fields 
            $this->load->model('DataPasienModel');
            $data = [
                "nama_pasien" => $_PUT['nama'],
                "alamat" => $_PUT['alamat'],
                "umur" => $_PUT['umur'],
                "no_hp" => $_PUT['no_hp'],
                "penjamin" => $_PUT['penjamin'],
                "tanggal_kunjungan" => $_PUT['tanggal_kunjungan'],
                "status" => $_PUT['status']
            ];
            if($this->DataPasienModel->update($_PUT['id'], $data)){
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    [
                        'success' => TRUE,
                        'message' => 'update successful'
                    ]
                ));
            } else {
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    [
                        'success' => FALSE,
                        'message' => 'update gagal'
                    ]
                ));
            }
        } else if($method == 'DELETE'){
            $this->load->model('DataPasienModel');
            if($this->DataPasienModel->delete($id)){
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    [
                        'success' => TRUE,
                        'message' => 'delete successful'
                    ]
                ));
            } else {
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    [
                        'success' => FALSE,
                        'message' => 'delete gagal'
                    ]
                ));
            }
        } else if($method == 'GET'){
            $this->load->model('DataPasienModel');
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(["data" => $this->DataPasienModel->get()->result_array()]));
        } else {
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(['error'=>'request is not found']));
        }
    }

    function dokter($id=NULL){
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == 'POST'){
            $this->load->model('DataDokterModel');
            $data = [
                'hari' => $this->input->post('hari'),
                'jam_buka' => $this->input->post('jam_buka'),
                'jam_tutup' => $this->input->post('jam_tutup'),
                'dokter' => $this->input->post('dokter'),
                'kuota' => $this->input->post('kuota')
            ];
            if($this->DataDokterModel->create($data)){
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    [
                        'success' => TRUE,
                        'message' => 'create sukses'
                    ]
                )); 
            } else {
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    [
                        'success' => FALSE,
                        'message' => 'create gagal'
                    ]
                ));
            }
        } else if($method == 'PUT') {
            $this->load->model('DataDokterModel');
            parse_str(file_get_contents('php://input'), $_PUT);
            $data = [
                "hari" => $_PUT['hari'],
                "jam_buka" => $_PUT['jam_buka'],
                "jam_tutup" => $_PUT['jam_tutup'],
                "dokter" => $_PUT['dokter'],
                "kuota" => $_PUT['kuota']
            ];
            if($this->DataDokterModel->update($_PUT['id'], $data)){
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    [
                        'success' => TRUE,
                        'message' => 'update successful'
                    ]
                ));
            } else {
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    [
                        'success' => FALSE,
                        'message' => 'update gagal'
                    ]
                ));
            }
        } else if($method == 'DELETE') {
            $this->load->model('DataDokterModel');
            if($this->DataDokterModel->delete($id)){
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    [
                        'success' => TRUE,
                        'message' => 'delete successful'
                    ]
                ));
            } else {
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    [
                        'success' => FALSE,
                        'message' => 'delete gagal'
                    ]
                ));
            }
        } else if($method == 'GET') {
            $this->load->model('DataDokterModel');
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(["data" => $this->DataDokterModel->get()->result_array()]));
        } else {
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(['error'=>'request is not found']));
        }
    }

    function riwayat_antrian($id=NULL){
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == 'POST'){
            $this->load->model('DataRiwayatAntrianModel');
            // cek terlebih dahulu, apakah nama pasien sudah di inputkan atau belum
            if($this->DataRiwayatAntrianModel->getTodayWhere($this->input->post('nama'), $this->input->post('tanggal'))->num_rows() >= 1){
                // jika sudah, berikan pesan bahwa data sudah ada
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(
                        [
                            'success' => FALSE,
                            'message' => 'Data telah ada'
                        ]
                    )); 
            } else {
                // jika belum, lakukan input
                $data = [
                    'nama' => $this->input->post('nama'),
                    'nomor' => $this->input->post('nomor'),
                    'bt' => $this->input->post('bt'),
                    'at' => $this->input->post('at'),
                    'tanggal' => $this->input->post('tanggal')
                ];
                if($this->DataRiwayatAntrianModel->create($data)){
                    $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(
                        [
                            'success' => TRUE,
                            'message' => 'create sukses'
                        ]
                    )); 
                } else {
                    $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(
                        [
                            'success' => FALSE,
                            'message' => 'create gagal'
                        ]
                    ));
                }
            }
        } else if($method == 'PUT'){
            $this->load->model('DataRiwayatAntrianModel');
            parse_str(file_get_contents('php://input'), $_PUT);
            $data = [
                "nama" => $_PUT['nama'],
                "nomor" => $_PUT['nomor'],
                "bt" => $_PUT['bt'],
                "at" => $_PUT['at'],
                "tanggal" => $_PUT['tanggal']
            ];
            if($this->DataRiwayatAntrianModel->update($_PUT['id'], $data)){
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    [
                        'success' => TRUE,
                        'message' => 'update successful'
                    ]
                ));
            } else {
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    [
                        'success' => FALSE,
                        'message' => 'update gagal'
                    ]
                ));
            }
        } else if($method == 'DELETE'){
            $this->load->model('DataRiwayatAntrianModel');
            if($this->DataRiwayatAntrianModel->delete($id)){
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    [
                        'success' => TRUE,
                        'message' => 'delete successful'
                    ]
                ));
            } else {
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                    [
                        'success' => FALSE,
                        'message' => 'delete gagal'
                    ]
                ));
            }
        } else if($method == 'GET'){
            $this->load->model('DataRiwayatAntrianModel');
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(["data" => $this->DataRiwayatAntrianModel->get()->result_array()]));
        }
    }

    function antrian($id=NULL){
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == 'GET'){
            $this->load->model('DataAntrianModel');
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(["data" => $this->DataAntrianModel->get()->result_array()]));
        } else if($method == 'PUT'){
            $this->load->library('AlgoritmaMFQ');
            $this->load->model(['DataRiwayatAntrianModel','DataAntrianModel']);
            $quantum = $this->config->item('quantum');
            $riwayatAntrianToday = $this->DataRiwayatAntrianModel->getToday()->result_array();
            
            $this->algoritmamfq->setDebug(FALSE);

            ##1 -- Menentukan 'Running Proses'
            $rp = $this->algoritmamfq->runningProses($riwayatAntrianToday, $quantum);
            ##2 -- Menentukan 'Gantt Chart'
            $arr = $this->algoritmamfq->urutkanAsQuantum($rp, $quantum);
            $waktuM = $this->algoritmamfq->waktuMulai($arr);
            $waktuA = $this->algoritmamfq->waktuBerakhir($arr);
            ##3 -- Menentukan 'Waiting Time'
            $wt = $this->algoritmamfq->waktuTunggu($riwayatAntrianToday, $quantum, $waktuM, $waktuA);
            ##4 -- Menentukan 'Turn Around Time'
            $ta = $this->algoritmamfq->waktuKirakira($riwayatAntrianToday, $wt);
            ##Langkah terakhir
            ##Menggabungkan semua data dari proses sebelumnya, kemudian diurutkan (Ascending) berdasarkan TA
            $all = $this->algoritmamfq->gabunganSemuaProses($riwayatAntrianToday, $waktuM, $wt, $ta);

            $datas = [];
            $i = 0;
            foreach($all as $v){
                array_push($datas, [
                    'antrian_pasien' => ++$i,
                    'antrian_admin' => $this->session->id,
                    'nomor' => $v['nomor'],
                    'tanggal' => $v['tanggal']
                ]);
            }
            
            if($this->DataAntrianModel->deleteAll() && $this->DataAntrianModel->createBatch($datas)){
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(
                        [
                            'success' => TRUE,
                            'message' => 'update sukses'
                        ]
                    ));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(
                        [
                            'success' => FALSE,
                            'message' => 'update gagal'
                        ]
                    ));
            }
        }
    }
}